import zmq
import time 

context = zmq.Context()
socket = context.socket(zmq.SUB)

socket.connect("tcp://zmq.devnet.iota.org:5556")

socket.subscribe("tx")
socket.subscribe("sn")
socket.subscribe("lmhs") 
print("Socket connected")

conf1 = b"ATLASCONFONE999999999999999"
conf2 = b"ATLASCONFTWO999999999999999"
init  = b"ATLASINIT999999999999999999"
spamtag = b"ATLASSPAM999999999999999999"
txs = [] 
bstx = [] 
ctxs = 0
globalSplit = 0 
globalSpam = 0 

while True:
    while True:
        message = socket.recv()
        data = message.split()
        if (data[0]) == b"tx":
            if (data[12] == conf1 or data[12] == conf2 or data[12] == init):
                globalSplit += 1 
            else: 
                if (data[12] == spamtag): 
                    globalSpam += 1
                txs.append((data[12], data[1]))
                bstx.append(data[1])
        elif (data[0]) == b"sn":
            if (data[2] in bstx):
                ctxs += 1
        elif (data[0]) == b"lmhs": 
            break 
    print("writing to file")
    f = open("data.csv","a")
    spam = 0
    split = globalSplit 
    myspam = globalSpam 
    for tx in txs: 
        if (tx[0]) == conf1: 
            split += 1 
        elif (tx[0]) == conf2: 
            split += 1 
        else: 
            spam += 1
    f.write(str(time.time()) + "," + str(spam) + "," + str(myspam) + "," + str(split) + "," +  str(ctxs) + "\n") 
    f.close()
    ctxs = 0 
    globalSpam = 0
    globalSplit = 0
    txs = [] 
    print("going back to watching") 
    #check confirmation
