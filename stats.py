import zmq
import time 

context = zmq.Context()
socket = context.socket(zmq.SUB)

socket.connect("tcp://35.233.51.156:5556")

socket.subscribe("tx")
socket.subscribe("sn")
socket.subscribe("lmhs") 
print("Socket connected")

txs = [] 
bstx = [] 
ctxs = 0
globalSplit = 0 

while True:
    while True:
        message = socket.recv()
        data = message.split()
        if (data[0]) == b"tx":
            if (data[12] == b"SCND99999999999999999999999" or data[12] == b"FRST99999999999999999999999" or data[12] == b"INIT99999999999999999999999"):
                globalSplit += 1 
            else: 
                txs.append((data[12], data[1]))
                bstx.append(data[1])
        elif (data[0]) == b"sn":
            if (data[2] in bstx):
                ctxs += 1
        elif (data[0]) == b"lmhs": 
            break 
    print("writing to file")
    f = open("data.csv","a")
    spam = 0
    split = globalSplit 
    for tx in txs: 
        if (tx[0]) == b"ATLAS": 
            myspam += 1 
        elif (tx[0]) == b"SCND":
            split += 1 
        elif (tx[0]) == b"FRST":
            split += 1 
        else:
            spam += 1
    f.write(str(time.time()) + "," + str(spam) + "," + str(myspam) + "," +  str(split) + "," +  str(ctxs) + "\n") 
    f.close()
    ctxs = 0 
    globalSplit = 0
    txs = [] 
    print("going back to watching") 
    #check confirmation
