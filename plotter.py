import matplotlib.pyplot as plt 
import csv 

time    = []
spam    = [] 
split   = []
confirm = [] 

clear = [] 
cloudy = [] 

with open(input("enter filename:\n")) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line = 0
    start = 0 
    for row in csv_reader: 
        if line == 0 : 
            start = float(row[0])
        line += 1 
        time.append(float(row[0]) - start) 
        spam.append(int(row[1]))
        if int(row[2]) == 0: 
            clear.append(int(row[4])/int(row[1]))
        else: 
            cloudy.append(int(row[4])/int(row[1]))
        split.append(int(row[3]))
        confirm.append(int(row[4])) 

if len(clear) > 0: 
    print("clear ", sum(clear)/len(clear))
if len(cloudy) > 0: 
    print("cloudy ", sum(cloudy)/len(cloudy))

def toSeconds (values):
    newList = []
    for i in values: 
        newList.append(i/90)
    return newList


xSpam = 0
xConf = 0
ySpam = 0
yConf = 0
AttackCount = 0
xCount = 0
yCount = 0 

for i, j, k in zip(split, spam, confirm):
    if i > 0: 
        xCount += 1
        xSpam += j
        xConf += k
        AttackCount += i
    else: 
        yCount += 1 
        ySpam += j
        yConf += k 

if xCount > 0: 
    print("atxps: ", (AttackCount / xCount)/90)
    print("a ratio: ", xConf / xSpam)

if yCount > 0: 
    print("n ration: ", yConf / ySpam) 

time1 = time 
spam1 = spam 
split1 = split 
confirm1 = confirm 


plt.subplot(2,1,1)
plt.plot(time1, toSeconds(spam1), 'b', label="Honest transactions") 
plt.plot(time1, toSeconds(split1), 'r', label="Attacker transactions")
plt.plot(time1, toSeconds(confirm1), 'g', label="Honest transactions confirmed") 
plt.title("With Described Attack Methodology - ")
plt.ylabel("Txs per second")
plt.xlabel("Seconds")

time    = []
spam    = [] 
split   = []
confirm = [] 

clear = [] 
cloudy = [] 

with open(input("enter filename:\n")) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line = 0
    start = 0 
    for row in csv_reader: 
        if line == 0 : 
            start = float(row[0])
        line += 1 
        time.append(float(row[0]) - start) 
        spam.append(int(row[1]))
        if int(row[2]) == 0: 
            clear.append(int(row[4])/int(row[1]))
        else: 
            cloudy.append(int(row[4])/int(row[1]))
        split.append(int(row[3]))
        confirm.append(int(row[4])) 

if len(clear) > 0: 
    print("clear ", sum(clear)/len(clear))
if len(cloudy) > 0: 
    print("cloudy ", sum(cloudy)/len(cloudy))

plt.subplot(2,1,2)
plt.plot(time, toSeconds(spam), 'b', label="Honest transactions") 
plt.plot(time, toSeconds(split), 'r', label="Attacker transactions")
plt.plot(time, toSeconds(confirm), 'g', label="Honest transactions confirmed") 
plt.title("No Intervention")
plt.ylabel("Txs per second")
plt.xlabel("Seconds")
plt.show()
