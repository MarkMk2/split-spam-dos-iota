import iota 
import time
from threading import Thread 
from pprint import pprint 
from datetime import datetime 

cooNode         = "http://146.148.24.138:14265"
testNode1       = "http://23.251.134.85:14265"
testNode2       = "http://35.187.109.69:14265"

originSeed      = "SEED99999999999999999999999999999999999999999999999999999999999999999999999999999"
conflictSeed    = "ASDSDDSADAVBGRGBGBGRBDSADSDQGFFGGREGREGREGVDVXCPOPPOMPMLKNLKMPOYTJYTHQZXCSCHRTH99"
conflictAltpoint= "SADSADHYHHYWDQWDBBFBVCKMNMONOLYTNHYTNHTLKHYTLKHTLKHNYTLKHNYTLKNHYTLKHLKYTNHTYHH99"
conflictEndpoint= "QWQWQWQWQWQWQWQEWTWETRETRETRETRETUYTUYTUUUYTUYTUTIUOIUOIUOIOPOIPOIPOIP99999999999"


api_origin      = iota.Iota(cooNode, seed = originSeed)

api_inter1      = iota.Iota(testNode1, seed = conflictSeed)
api_inter2      = iota.Iota(testNode2, seed = conflictSeed)

api_end1        = iota.Iota(testNode1, seed = conflictEndpoint)
api_end2        = iota.Iota(testNode2, seed = conflictAltpoint) 

print("Setting addresses...")
interAddr       = (api_inter1.get_new_addresses(index = 0, count = 4, security_level = 2))["addresses"]
set1Addr        = (api_end1.get_new_addresses(index = 0, count = 4, security_level = 2))["addresses"]
set2Addr        = (api_end2.get_new_addresses(index = 0, count = 4, security_level = 2))["addresses"] 
print("Addresses set.")

# Set up inital balance, send it to two different addresses simultaneously. Repeat. 
def split_it():

    while (True):    
        print("Checking intermediate balance...")
        while (api_inter1.get_account_data(start=0, stop=None)["balance"] != 0):
            time.sleep(1) # todo
            print("Waiting for intermediate to empty.")
        print("Intermediate balance was empty.") 

        # check initial balances of endpoints
        #initBalances = [api_end1.get_account_data(start=0, stop=None)["balance"], 
        #       api_end2.get_account_data(start=0, stop=None)["balance"]]
        
        # Send inital balance to intermediary address
        print("Preparing initial tx")
        txInit = iota.ProposedTransaction( address = iota.Address(interAddr[0]), message = None, 
                tag = iota.Tag(b'INIT99999999999999999999'),
                value   = 10)
        print("Sending initial tx...") 
        sentBundleInit  = api_origin.send_transfer (depth=3, transfers=[txInit], 
                inputs = None, change_address=None, 
                min_weight_magnitude=3, security_level=2)
        # I could send it to all nodes at this point
        print("Initial tx sent.") 
        

        # Could try checking number of init transactions, vs first/second at this point
        print("Checking node's sync...") 
        while (api_inter1.get_account_data(start=0, stop=None)["balance"] != 10) and (api_inter2.get_account_data(start=0, stop=None)["balance"] != 10):
            time.sleep(1) # todo 
            print("Waiting for nodes 1 and 2 to recognise intermediate's balance")
        
        print("Starting other threads...") 
        # Send that to the two different addresses on the different nodes. 
        Thread(target = split1).start()
        Thread(target = split2).start()


def split1():
    txSplit = iota.ProposedTransaction( address = iota.Address(set1Addr[0]), message = None, 
            tag = iota.Tag (b'FIRST9999999999999999999'),
            value = 10)
    print("made tx 1")
    sendBundleSplit =  api_inter1.send_transfer (depth=3, transfers=[txSplit], 
            inputs = None, change_address=None, 
            min_weight_magnitude=3, security_level=2)

def split2(): 
    txSplit = iota.ProposedTransaction( address = iota.Address(set2Addr[0]), message = None, 
            tag = iota.Tag (b'SECOND999999999999999999'),
            value = 10)
    print("made tx 2") 
    sendBundleSplit =  api_inter2.send_transfer (depth=3, transfers=[txSplit], 
            inputs = None, change_address=None, 
            min_weight_magnitude=3, security_level=2)


split_it()





