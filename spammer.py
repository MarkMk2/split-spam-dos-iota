#!/usr/bin/python

import iota 
import time 
import sys 
from threading import Thread
from pprint import pprint 
from datetime import datetime 

cooNode         = "http://35.233.51.156:14265"
testNode1       = "http://23.251.134.85:14265"
testNode2       = "." 


def spam_it():
    
    spam    = iota.ProposedTransaction(address = iota.Address(originAddr), # 81 trytes long address
                                        message = None,
                                        tag     = iota.Tag(b'LEFTSPAM9999999999999999'), # Up to 27 trytes
                                        value   = 0)
    pb          =   iota.ProposedBundle(transactions=[spam])
    pb.finalize()
    spamTrytes  =   pb.as_tryte_strings()
    
    
    def spam1():
        while (True): 
            spamGTA     =   api2.get_transactions_to_approve(depth = 2)
            spamAtt         =   api2.attach_to_tangle(trunk_transaction=spamGTA['trunkTransaction'],
                                       branch_transaction=spamGTA['branchTransaction'], # second tip selected
                                       trytes=spamTrytes, # our finalized bundle in Trytes
                                       min_weight_magnitude=3) # MWMN
            pprint(api2.broadcast_and_store (spamAtt ['trytes']))

    def spam2():
        while (True):
            spamGTA     =   api1.get_transactions_to_approve(depth = 2)
            spamAtt         =   api1.attach_to_tangle(trunk_transaction=spamGTA['trunkTransaction'], 
                                       branch_transaction=spamGTA['branchTransaction'], # second tip selected
                                       trytes=spamTrytes, # our finalized bundle in Trytes
                                       min_weight_magnitude=3) # MWMN
            pprint(api1.broadcast_and_store (spamAtt ['trytes']))
    
    while (True):
        t1 = Thread(target = (spam1), args = ())
        #t2 = Thread(target = (spam2), args = ())
        t1.start()
        #t2.start()
        t1.join()
        #t2.join() 

def main(): 
    
    global originSeed, conflictSeed, sampleSeed 
    originSeed      = "SEED99999999999999999999999999999999999999999999999999999999999999999999999999999"
    conflictSeed    = "ASDSDDSADAVBGRGBGBGRBDSADSDQGFFGGREGREGREGVDVXCPOPPOMPMLKNLKMPOYTJYTHQZXCSCHRTH99"
    sampleSeed      = "AAABBBCCCDDDEEEFFFGGGHHHIIIJJJKKKLLLMMMNNNOOOPPPQQQRRRSSSTTTUUUVVVWWWXXXYYYZZZ999"

    global apiCoo, api1, api2 
    apiCoo          = iota.Iota(cooNode, seed = originSeed)
    api1            = iota.Iota(testNode1, seed = conflictSeed)
    api2            = iota.Iota(cooNode, seed = sampleSeed) 

    global originAddr, conflictAddr 
    originAddr      = (apiCoo.get_new_addresses(index = 0, count = 3, security_level = 2))["addresses"][0]
    conflictAddr    = (apiCoo.get_new_addresses(index = 0, count = 3, security_level = 2))["addresses"][0]

    global spamAddr,spamEndpoint, spamStartpoint 
    spamAddr        = (apiCoo.get_new_addresses(index = 0, count = 3, security_level = 2))["addresses"]
    spamEndpoint    = spamAddr[1]
    spamStartpoint  = spamAddr[0]
    
    spam_it() 

if __name__ =='__main__':
    main()
