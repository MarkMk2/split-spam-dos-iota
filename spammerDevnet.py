#!/usr/bin/python

import iota 
import time 
import sys 
from threading import Thread
from pprint import pprint 
from datetime import datetime 
from iota import *
from iota.adapter.wrappers import RoutingWrapper 
devnetNode      = "https://nodes.devnet.thetangle.org:443"
spamSeed        = "ASDSDDSADAVBGRGBGBGRBDSADSDQGFFGGREGREGREGVDVXCPOPPOMPMLKNLKMPOYTJYTHQZXCSCHRTH99"
api             = iota.Iota(RoutingWrapper(devnetNode).add_route('attachToTangle', 'http://localhost:14265'), seed = spamSeed)
targetAddr      = (api.get_new_addresses(index = 0, count = 3, security_level = 2))["addresses"][0]


def spam_it():
    spam    = iota.ProposedTransaction(address = iota.Address(targetAddr), # 81 trytes long address
                                        message = None,
                                        tag     = iota.Tag(b'ATLASSPAM'), # Up to 27 trytes
                                        value   = 0)
    pb          =   iota.ProposedBundle(transactions=[spam])
    pb.finalize()
    spamTrytes  =   pb.as_tryte_strings()
    
    def spamMessage():
        while (True): 
            spamGTA         =   api.get_transactions_to_approve(depth = 2)
            spamAtt         =   api.attach_to_tangle(trunk_transaction=spamGTA['trunkTransaction'],
                                       branch_transaction=spamGTA['branchTransaction'], # second tip selected
                                       trytes=spamTrytes, # our finalized bundle in Trytes
                                       min_weight_magnitude=9) # MWMN
            pprint(api.broadcast_and_store (spamAtt ['trytes']))
    
    spamMessage()

def main(url1, url2, url3): 
    spam_it() 

if __name__ =='__main__':
    main(devnetNode, devnetNode, devnetNode)

